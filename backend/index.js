const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require("body-parser");

const app = express();
const baseUrl = '/apimotos';
app.use(cors());

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

ddbbConfig = {

   user: "albert-rodriguez-7e3",
   host: "postgresql-albert-rodriguez-7e3.alwaysdata.net",
   database: "albert-rodriguez-7e3_exfinalionic",
   password: "cao260801",
   port: 5432,

};

const Pool = require("pg").Pool;
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
//Ahora seria al acceder aquí: http://localhost:3000/apimotos/test

const getTest = (request, response) => {
   response.send('Hola test')
}

app.get(baseUrl + '/test', getTest);

const getMotos = (request, response) => {

   var cogerMotos = 'SELECT * FROM motos';
      pool.query(cogerMotos, (error, results) => {
         if (error) {
            throw error;
         }
         if (results.rowCount != 0){
      response.status(200).json(results.rows);
         } else {
            response.status(200).json({response: "No se encuentra la moto"})
         }
   });
//const mayus = (marcaMoto) => {return marcaMoto.charAt(0).toUpperCase() + marcaMoto.slice(1);
   
};

let sentencia;



app.get(baseUrl + "/getmotos", getMotos);

const borrarMoto = (request, response) => {const motoId = request.params.id;

   var consulta = `DELETE FROM motos WHERE id=${motoId};`;
   pool.query(consulta, (error, results) => {

      if (error) {
         throw error;
      }
      if (results.rowCount != 0){
         response.status(200).json(results.rows);
      } else {
         response.status(200).json({response: "No se encontró la moto deseada"});

      }
   });
};


const filtrarMoto = (request, response) => {const motoId = request.params.id;

   
      consulta = `SELECT * FROM motos WHERE marca='${request.query.marca}';`;
   
   pool.query(consulta, (error, results) => {

      if (error) {
         throw error;
      }
      if (results.rowCount != 0){
         response.status(200).json(results.rows);
      } else {
         response.status(200).json({response: "No se encontró la moto deseada"});

      }
   });
};

const altaMoto = (request, response) => {

   const { marca, modelo, ano, precio} = request.body;
   var subir = `insert into motos (marca, modelo, ano, precio) values ('${marca}','${modelo}','${ano}','${precio}');`

   pool.query(subir, (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).json(results.rows);
    });

   console.log(request.body);
   //response.send("Respuesta: " + JSON.stringify(request.body));

};

app.post(baseUrl + "/moto", altaMoto);
app.delete(baseUrl + "/borrarMoto/:id", borrarMoto);
app.get(baseUrl + "/filtrarMoto", filtrarMoto);

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
